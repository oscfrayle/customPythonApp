from simplerouter import Router
from app_customers.urls import url_pattern_customer


class MainRouter(object):
    def __init__(self):
        self.router = Router(default="module:error_view")
        for url in url_pattern_customer:
            self.router.add_route(url[0], url[1])

    def get_router_app(self):
        return self.router
