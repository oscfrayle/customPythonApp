'''
    file for base configurations, core settings project
'''

import os
import sys
import env
from config.wsgi import InitProject
from config.urls import MainRouter

debug = True

app_router = MainRouter()
application = app_router.get_router_app().as_wsgi
InitProject(env.HOST, env.PORT, application).run_server()


