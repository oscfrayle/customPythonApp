from wsgiref.simple_server import make_server


class InitProject(object):
    def __init__(self, host, port, application):
        self.host = host
        self.port = port
        self.application = application

    def run_server(self):
        make_server('', self.port, self.application).serve_forever()
