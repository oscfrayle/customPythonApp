from webob import Response
from app_customers.mixins import RequestMixin


from datetime import date
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app_customers.models import Customers

# conexion
engine = create_engine('sqlite:///customers.db', echo=True)
# sesion
Session = sessionmaker(bind=engine)
session = Session()


class CustomersView(RequestMixin):
    '''
            Customers view expose de principal verbose for REST API
    '''

    def resources(self, request):
        method = self.strategy_endpoint(request)
        response = eval('self.%s' % method)(request)
        return Response(response)

    def get(self, request):
        '''
        :param request:
        :return:
        '''
        # print(request.GET['id'])
        # post_name = request.urlvars['id']
        # print(post_name)

        return Response("Site index with class")

    def post(self, request):
        '''
        :param request:
        :return:
        '''
        print(request.POST['id'])
        customer = session.query(Customers).all()
        print(customer)
        # post_name = request.urlvars['id']
        # print(post_name)
        return 'este es el m post'

    def put(self, request):
        '''
        :param request:
        :return:
        '''
        print(request.GET['id'])
        post_name = request.urlvars['id']
        print(post_name)
        return Response("Site index with class")

    def delete(self, request):
        '''
        :param request:
        :return:
        '''
        print(request.GET['id'])
        post_name = request.urlvars['id']
        print(post_name)
        return Response("Site index with class")

