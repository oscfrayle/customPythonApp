import re


class RequestMixin(object):
    method = ''

    def strategy_endpoint(self, req):
        if re.findall("GET", str(req)):
            self.method = 'get'
        if re.findall("POST", str(req)):
            self.method = 'post'
        if re.findall("DELETE", str(req)):
            self.method = 'delete'
        return self.method
