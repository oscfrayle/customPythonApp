class ResourcesException(object):
    pass


class PostException(object):
    pass


class GetException(object):
    pass


class DeleteException(object):
    pass


class UpdateException(object):
    pass
