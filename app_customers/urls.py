from app_customers.views import CustomersView
cus = CustomersView()

url_pattern_customer = (
        ('/customer/', CustomersView().resources),
        ('/customer1/{id}', CustomersView().post),
        # ('/customer1/{id}', CustomersView().get),
        # ('/customer/{id}',  CustomersView().get),
        ('/', 'views:index_view'),
    )
